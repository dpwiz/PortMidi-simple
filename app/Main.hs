{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE LambdaCase #-}

module Main where

import System.Environment (getArgs)

import qualified Sound.PortMidi.Simple as Midi

main :: IO ()
main = do
  getArgs >>= \case
    [] -> do
      devices <- Midi.getDevices
      mapM_ print devices
    some ->
      example $ unwords some

example :: [Char] -> IO ()
example name =
  Midi.withMidi $
    Midi.findInputNamed name >>= \case
      Nothing ->
        fail $ "Input device not found: " <> show name
      Just device -> do
        putStrLn $ "Dumping from " <> show name
        Midi.withInput device \stream ->
          Midi.withReadMessages stream 256 \readMessages -> do
            putStrLn "Tweak knobs and press Enter..."
            _ <- getLine
            readMessages >>= mapM_ print
