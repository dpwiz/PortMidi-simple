# Changelog for PortMidi-simple

## Unreleased changes

## [0.1.0.1] - 2021-06-19

* Executable example put under a flag.

[0.1.0.1]: https://gitlab.com/dpwiz/PortMidi-simple/tree/v0.1.0.1

## [0.1.0.0] - 2021-02-27

* Initial import

[0.1.0.0]: https://gitlab.com/dpwiz/PortMidi-simple/tree/v0.1.0.0
