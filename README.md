# PortMidi-simple 🚢🎶

> Reading from MIDI controllers should not be too difficult.

```haskell
{-# LANGUAGE BlockArguments #-}
import qualified Sound.PortMidi.Simple as Midi

main = Midi.withMidi do
  Just device <- Midi.findInputNamed "nanoKONTROL2"
  Midi.withInput device \stream ->
    Midi.withReadMessages stream 256 \readMessages ->
      forever do
        readMessages >>= mapM_ print
        threadDelay 1000
```
